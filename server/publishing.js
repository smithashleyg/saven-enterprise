//
/*DASHBOARD COUNTS */
//
Meteor.publish('totalBroadcasts', function() {
  if(this.userId){
    var organisation = Meteor.user().profile.organisation;
  
  if(organisation!='administrator'){
    Counts.publish(this, 'totalBroadcasts', Broadcasts.find({
        organisation: organisation
    }));
  }else{
    Counts.publish(this, 'totalBroadcasts', Broadcasts.find({}));
    }
    }
});


Meteor.publish("orgByGps", function(long,lat){
   if(this.userId){
  var organisation = Meteor.user().profile.organisation;
  Counts.publish(this, 'totalPriorityBroadcasts', Broadcasts.find({
      organisation: organisation,
      priority:true
  }));

}
});


Meteor.publish('userByID', function(id) {
 if(this.userId){
    return Meteor.users.find({
        _id: id
    });
  }
});

Meteor.publish('allUsers', function() {
 if(this.userId){
    return Meteor.users.find({});
  }
});


Meteor.publish('broadcastsByUser', function(id) {
   if(this.userId){
    return Broadcasts.find({user_id:id});
  }
});

Meteor.publish('refers', function() {
   if(this.userId){
    return Refers.find({});
  }
});



Meteor.publish('allUserFields', function() {
   if(this.userId){
  var organisation = Meteor.user().profile.organisation;
  if(organisation=='administrator'){
  return Userfields.find({});
}else{
  return Userfields.find({
      organisation: organisation
  });
}
}

});



Meteor.publish("organisations", function() {
    if (this.userId) {
      var organisation = Meteor.user().profile.organisation;
  if(organisation=='administrator'){
    return Organisations.find({});
  }else{
        return Organisations.find({
            organisation: organisation
        });
      }
    } else {
        this.ready();
    }

});

Meteor.publish("organisationsByID", function(key) {
   if(this.userId){
  return Organisations.find({
            key:key
        });
}

});

Meteor.publish("savenLogger", function() {
   if(this.userId){
    if (this.userId) {
var organisation = Meteor.user().profile.organisation;

  if(organisation=='administrator'){
                  return Savenlogger.find({}, {
            skip: 0,
            limit: 7,
            sort: {
                dateTime: -1
            }});
  }else{
        return Savenlogger.find({
            organisation: organisation
        },{skip: 0, limit: 7, sort: {dateTime: -1}}  );
      }
    } else {
        this.ready();
    }
}
});

Meteor.publish("savenLoggerByUser", function(user_id) {
    if (this.userId) {
        return Savenlogger.find({
            user_id: user_id
        });
    } else {
        this.ready();
    }

});
