Meteor.methods({
    'dashboardCounts': function() {

        var organisation = Meteor.user().profile.organisation;

        var curr = new Date;
        var startOfDay = moment().startOf('day').toDate();
        var endOfDay = moment().endOf('day').toDate();

        var startOfMonth = moment().startOf('month').toDate();
        var endOfMonth = moment().endOf('month').toDate();

        var startOfWeek = moment().startOf('week').toDate();
        var endOfWeek = moment().endOf('week').toDate();

         var broadcastsDailyFilter = {
                "createDateTime": {
                    $gt: startOfDay,
                    $lt: endOfDay
                },
                "loc": { $exists: true },
                "loc": {
                    $geoWithin: {
                        $geometry: {
                            type: "Polygon",
                            coordinates: getGeoCoordinates()
                        }
                    }
                }
            }

         var broadcastsWeeklyFilter = {
                "createDateTime": {
                    $gt: startOfWeek,
                    $lt: endOfWeek
                },
                "loc": { $exists: true },
                "loc": {
                    $geoWithin: {
                        $geometry: {
                            type: "Polygon",
                            coordinates: getGeoCoordinates()
                        }
                    }
                }
            }

             var broadcastsMonthlyFilter = {
                "createDateTime": {
                    $gt: startOfMonth,
                    $lt: endOfMonth
                },
                "loc": { $exists: true },
                "loc": {
                    $geoWithin: {
                        $geometry: {
                            type: "Polygon",
                            coordinates: getGeoCoordinates()
                        }
                    }
                }
            }

        var broadcastsDaily = Broadcasts.find(broadcastsDailyFilter).count();
        var broadcastsWeekly = Broadcasts.find(broadcastsWeeklyFilter).count();
        var broadcastsMonthly = Broadcasts.find(broadcastsMonthlyFilter).count();

     return {daily: broadcastsDaily, weekly: broadcastsWeekly, monthly: broadcastsMonthly}

        

    }

});