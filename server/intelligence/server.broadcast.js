Meteor.methods({
    getBroadcastsMethod: function(data) {
        var organisation = Organisations.findOne({ 'organisation': Meteor.user().profile.organisation }, {
            fields: {
                'polygons': 1
            }
        });

        if (Meteor.user().profile.organisation == 'administrator') {
            var query = {
                "loc": { $exists: true },
            }
        } else {
            var query = {
                "loc": { $exists: true },
                "loc": {
                    $geoWithin: {
                        $geometry: {
                            type: "Polygon",
                            coordinates: getGeoCoordinates()
                        }
                    }
                }
            }
        }

        var b = Broadcasts.find(query, {
            fields: {
                'images': 0,
                'sentTo': 0
            }
        }).fetch();

        return b


    },
        getBroadcastDashboardCounts: function(data) {
        var organisation = Organisations.findOne({ 'organisation': Meteor.user().profile.organisation }, {
            fields: {
                'polygons': 1
            }
        });


var startOfDay = new Date();
startOfDay.setHours(0,0,0,0);

var endOfDay = new Date();
endOfDay.setHours(23,59,59,999);


var startOfWeek = moment().startOf('week').toDate();
var endOfWeek   = moment().endOf('week').toDate();


        if (Meteor.user().profile.organisation == 'administrator') {


            var queryToday = {
                "createDateTime":{$gte: startOfDay, $lt: endOfDay},
                "loc": { $exists: true },
                "loc": {
                    $geoWithin: {
                        $geometry: {
                            type: "Polygon",
                            coordinates: getGeoCoordinates()
                        }
                    }
                }
            }

            // var queryWeek = {
            //     "createDateTime":{$gte: startOfWeek, $lt: endOfWeek},
            //     "loc": { $exists: true },
            //     "loc": {
            //         $geoWithin: {
            //             $geometry: {
            //                 type: "Polygon",
            //                 coordinates: getGeoCoordinates()
            //             }
            //         }
            //     }
            // }

        var todayCount = Broadcasts.find(queryToday).count()
        // var weekCount = Broadcasts.find(queryWeek).count()

        console.log('COUNT: ', todayCount)

        return todayCount

}

    }
});