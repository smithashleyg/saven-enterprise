    Meteor.methods({
    'deleteBroadcast': function(data) {
        var user = Meteor.user();
        var fullname = user.profile.fullname;
        var userId = data._id;
        

        if (user && (user.isAdmin === true)) {

            Broadcasts.remove({'_id':data._id});


        } else {
            throw new Meteor.Error(403, 'Forbidden');
        }
       
    },        'getBroadcastbyID': function(id) {
        var user = Meteor.user();

        if (user) {

   
   return Broadcasts.find({ _id: id }, {
        fields: {
            reportType: 1,
            content: 1,
            fullname:1,
            broadcaster:1,
            suburb: 1,
            createDateTime:1,
            geoData:1,
            broadcastOtherLocation:1,
            loc:1
        }
    }).fetch();


        } else {
            throw new Meteor.Error(403, 'Forbidden');
        }
       
    },
        'getBroadcastForModal': function(data) {
        var user = Meteor.user();

        if (user) {

   
   return Broadcasts.find({ _id: { $in: data } }, {
        fields: {
            reportType: 1,
            content: 1,
            suburb: 1,
            createDateTime:1
        }
    }).fetch();


        } else {
            throw new Meteor.Error(403, 'Forbidden');
        }
       
    }
});





