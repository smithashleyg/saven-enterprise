Template.dashboard.onCreated(function() {
    Session.set('broadcastBySuburbReady', false);
    GoogleMaps.ready('map', function(map) {

        Meteor.call('getBroadcastsMethod', null, function(error, result) {

            if (error) {
                console.error(error.reason);
                return;
            }

            var infoWin = new google.maps.InfoWindow();

            Session.set('getBroadcastData', result);
            Session.set('broadcastBySuburbReady', true);

            var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var broadcasts = [];
            var getBroadcastData = Session.get('getBroadcastData')

            getBroadcastData.forEach(function(item) {
                broadcasts.push([item.loc.coordinates[1], item.loc.coordinates[0], item._id])
            });

            var markers = broadcasts.map(function(broadcast, i) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(broadcast[0], broadcast[1]),
                    setTitle: broadcast[2]

                });






                google.maps.event.addListener(marker, 'click', function(evt) {

                    Meteor.call('getBroadcastbyID', broadcast[2], function(error, result) {
                        if (result) {

                            var message = result[0].content;

                            if (message.length > 200) {
                                var message = message.substring(0, 200) + '...';
                            } else {
                                var message = result[0].content;
                            }

                            $('#broadcastModal').modal('open');
                            console.log(result[0])

                            $('#reportType').html(result[0].reportType)
                            $('#content').html(message)
                            $('#fullname').html(result[0].fullname)
                            $('#createDateTime').html(result[0].createDateTime)
                            $('#suburb').html(result[0].suburb)

                            if(result[0].geoData){
                                $('#location').html(result[0].geoData.formatted_address)
                            }else if (result[0].broadcastOtherLocation){
                                $('#location').html(result[0].broadcastOtherLocation)
                            }else{
                                $('#location').html('-')
                            }
                            

                            $("#viewBroadcastLink").prop("href", "/viewbroadcast/" + result[0]._id);

// if(result[0].loc){

//     lng = result[0].loc.coordinates[0];
//     lat = result[0].loc.coordinates[1];

//    var fenway = {lat: lat, lng: lng};
// var panoramaOptions = {
//   addressControl: false,
//   position: fenway,
//   pov: {
//     heading: 0, 
//     pitch: -40, 
//   },
//   visible: true
// };
// var streetview = new google.maps.StreetViewPanorama(document.getElementById("streetview"),
//                      panoramaOptions);
//                         }

}

                    });
                })
                return marker;
            });



            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map.instance, markers, {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            });


            google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {
                var content = '';

                // Convert lat/long from cluster object to a usable MVCObject
                var info = new google.maps.MVCObject;
                info.set('position', cluster.center_);

                //----
                //Get markers
                var markers = cluster.getMarkers();

                var titles = [];
                //Get all the titles
                for (var i = 0; i < markers.length; i++) {

                    //titles += markers[i].setTitle + "\n";   
                    titles.push(markers[i].setTitle)
                }


                if (cluster.getMap().getZoom() > 15) {
                    $('#broadcastModalList').modal('open');

                    var broadcastIDs = titles.map(function(item) {
                        return item;
                    });


                    Meteor.call('getBroadcastForModal', broadcastIDs, function(error, result) {

                        if (!error) {
                            Session.set('getBroadcastForModal', result)

                        } else {
                            console.error(error)
                        }
                    });

                }

            });


        });

    });
});


Template.dashboard.helpers({
    getBroadcastForModal: function() {
        return Session.get('getBroadcastForModal')
    },
    broadcastBySuburbReady: function() {

        return Session.get('broadcastBySuburbReady')

    },
    mapOptions: function() {
        var polygon = Organisations.findOne({});

        if (GoogleMaps.loaded() && polygon) {
            Session.set('broadcastBySuburbReady', true);
            var latLng = Location.getReactivePosition();
            return {
                center: new google.maps.LatLng(polygon.polygons.coordinates[0][0][1], polygon.polygons.coordinates[0][0][0]),
                zoom: 12,
                scrollwheel: false,
                styles: [{
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#f5f5f5"
                        }]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#616161"
                        }]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [{
                            "color": "#f5f5f5"
                        }]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#bdbdbd"
                        }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#eeeeee"
                        }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#757575"
                        }]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#e5e5e5"
                        }]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#9e9e9e"
                        }]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#ffffff"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#757575"
                        }]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#dadada"
                        }]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#616161"
                        }]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#9e9e9e"
                        }]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#e5e5e5"
                        }]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#eeeeee"
                        }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#c9c9c9"
                        }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#9e9e9e"
                        }]
                    }
                ]

            };
        }
    }
});