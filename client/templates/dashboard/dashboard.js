Template.dashboard.onCreated(function() {

    Meteor.call('dashboardCounts', null, function(error, result) {

        if (error) {
            console.error(error);
            return;
        }

        console.log(result)


        Session.set('dashboardCounts', result);


    });

    // GoogleMaps.ready('map', function(map) {

    //     var orgCode = Meteor.user().profile.organisation;
    //     var polygon = Organisations.findOne({organisation: orgCode});
    //     var polylineExists = Organisations.find({ organisation: orgCode, "polygons": { $exists: true } }).count();

    //     function addPolygon(coords) {
    //         var polygon_coordinates = [];
    //         for (var i = 0; i < coords.length; i++) {
    //             polygon_coordinates.push({
    //                 lat: parseFloat(coords[i][1]),
    //                 lng: parseFloat(coords[i][0])
    //             });
    //         }

    //         var new_polygon = new google.maps.Polygon({
    //             paths: polygon_coordinates,
    //             strokeColor: '#2996c0',
    //             strokeOpacity: 0.8,
    //             strokeWeight: 2,
    //             fillColor: '#2996c0',
    //             fillOpacity: 0.35

    //         });
    //         new_polygon.setMap(map.instance);
    //     }

    //     if(polylineExists){

    //     addPolygon(polygon.polygons.coordinates[0]);
    // }

    // });
});


Template.dashboard.events({
  'click .reactive-table tbody tr': function (event) {
    event.preventDefault();

         Router.go("/viewbroadcast/"+this._id); 
        }

});

Template.dashboard.helpers({
    dashboardCount: function(){
        return Session.get('dashboardCounts');
    },
    broadcasts: function() {

        return Broadcasts.find({}, { sort: { createDateTime: -1 } });

    },
    settings: function() {
        return {
            showNavigation: 'auto',
            noDataTmpl: 'noDataTmpl',
            rowsPerPage: 10,
            showFilter: false,
            fields: [{
                    key: 'createDateTime',
                    label: 'Date / Time',
                     sortDirection: 1,
                    fn: function(value, object, key) { return moment(value).format('MM/DD/YYYY, h:mm a'); }

                },
                {
                    key: 'reportType',
                    label: 'Type',
                    fn: function(value, object, key) { return value.toUpperCase() }
                },

                {
                    key: 'suburb',
                    label: 'Suburb',
                    fn: function(value, object, key) { return value.toUpperCase() }

                }
            ]
        };
    }
});