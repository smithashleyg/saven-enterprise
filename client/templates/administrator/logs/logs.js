Template.logs.onCreated(function() {

    this.subscribe('savenlogs');

});


// Template.logs.events({
//     'click #location': function(e) {
//         e.preventDefault();

//     }
// });

Template.logs.helpers({
    savenlogs: function() {
        return Savenlogs.find({},{$sort: {dateTime: -1}});
    }
});

Template.logs.helpers({
    settings: function() {
        return {

            rowsPerPage: 100,
            showFilter: true,
            fields: [{
                    key: 'event',
                    label: 'Event',
                },
                            {
                    key: 'fullname',
                    label: 'Fullname',
                } ,
                                            {
                    key: 'message',
                    label: 'Error',
                },
                {
                    key: 'source_id',
                    label: 'Source ID',
                } ,
            {
                    key: 'dateTime',
                    label: 'Date / Time',
                } 



            ]
        };
    }
});

// Template.logs.events({
//     'click .reactive-table tbody tr': function(event) {
//         event.preventDefault();
//         var user = this;

//         if (event.target.className != "delete") {
//             Router.go("/userprofile/" + user._id);
//         }


//     }
// });