Template.broadcasttypes.onCreated(function() {


    Meteor.call('broadcasttypes', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        var chart = new Chartist.Bar('.broadcasttypes', result, {
            distributeSeries: true,
            fullWidth: true,
             low: 0,
             onlyInteger: true,
             reverseData: true,
              horizontalBars: true,
               height: '500px',
               axisY: {
    offset: 70,
    onlyInteger: true
  },
                 axisX: {
    onlyInteger: true
  }
        });

        Session.set('broadcasttypes', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });

});





Template.broadcasttypes.onRendered(function() {


  var getBarChartData = Session.get('broadcasttypes')


  new Chartist.Bar('.broadcasttypes', {getBarChartData}, {
    fullWidth: true,
    showArea: true,
    chartPadding: {
      right: 40
    }
  });


});


Template.broadcasttypes.helpers({
    barchartDataReady: function() {

        return Session.get('barchartDataReady')

    }

});
