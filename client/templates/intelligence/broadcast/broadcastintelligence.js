import Chart from 'chart.js'



    Template.broadcastintelligence.rendered = function(){


new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, { 
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, { 
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});



}


Template.broadcastintelligence.onCreated(function() {

        Meteor.call('dashboardCounts', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        console.log(result)


        Session.set('dashboardCounts', result);


    });

    Session.set('barchartDataReady', false);

    Meteor.call('broadcastIntelData', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }


        Session.set('broadcastIntelData', result);


    });

    Meteor.call('barchart', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        var chart = new Chartist.Bar('.broadcastMonthly', result, {
            distributeSeries: true
        });

        Session.set('barchart', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });

    Meteor.call('chartBroadcastTypes', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        var chart = new Chartist.Bar('.chartBroadcastTypes', result, {
            distributeSeries: true
        });

        Session.set('chartBroadcastTypes', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });


    Meteor.call('weeklyBroadcasts', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

 new Chartist.Line('.weeklyBroadcasts', {
  labels: result.labels,
  series: [
    result.series
  ]
}, {
  low: 0,
  showArea: true
});


        Session.set('weeklyBroadcasts', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });

});

Template.broadcastintelligence.helpers({
        dashboardCount: function(){
        return Session.get('dashboardCounts');
    },
    barchartDataReady: function() {

        return Session.get('barchartDataReady')

    },
    currentMonth: function() {

        return moment().format('MMMM');

    },
    currentYear: function() {

        return moment().format('YYYY');

    },


});