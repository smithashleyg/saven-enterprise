Template.userintelligence.onRendered(function() {

    
var data = {
  labels: ['Guardian','User'],
  series: [20, 15]
};

var options = {
  labelInterpolationFnc: function(value) {
    return value[0]
  }
};


new Chartist.Pie('.userType', data);

});

Template.userintelligence.onCreated(function() {





    Session.set('barchartDataReady', false);

    Meteor.call('userIntelData', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }


        Session.set('userIntelData', result);

    });

    Meteor.call('userBarchart', null, function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }
        
        var chart = new Chartist.Bar('.userMonthly', result, {
            distributeSeries: true
        });

        Session.set('barchart', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });

});

Template.userintelligence.helpers({
    userIntelData: function() {
        var v = Session.get('userIntelData');
        if (v) {
            return v;
        } else {
            return 0;
        }
    },
    barchartDataReady: function() {

        return Session.get('barchartDataReady')

    },
    currentMonth: function() {

        return moment().format('MMMM');

    },
    currentYear: function() {

        return moment().format('YYYY');

    },


});
