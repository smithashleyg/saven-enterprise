

Template.broadcastsuburbs.onCreated(function() {

//ReactiveVars
  this.fromFilter = new ReactiveVar("")
  this.toFilter = new ReactiveVar("")

    Meteor.call('broadcastsuburbs', function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        var chart = new Chartist.Bar('.broadcastsuburbs', result, {
            distributeSeries: true,
            fullWidth: true,
            low: 0,
            onlyInteger: true,
            reverseData: true,
            horizontalBars: true,
            height: '500px',
            axisY: {
                offset: 70,
                onlyInteger: true
            },
            axisX: {
                onlyInteger: true
            }
        });

        Session.set('broadcastsuburbs', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });
  });






Template.broadcastsuburbs.onRendered(function() {

var fromFilter       = this.fromFilter.get()
 var toFilter        = this.toFilter.get();

Tracker.autorun(() => {
 console.log('from Change 2', this.fromFilter.get())
    Meteor.call('broadcastsuburbs' ,this.fromFilter.get(),this.toFilter.get(),function(error, result) {

        if (error) {
            console.error(error.reason);
            return;
        }

        var chart = new Chartist.Bar('.broadcastsuburbs', result, {
            distributeSeries: true,
            fullWidth: true,
            low: 0,
            onlyInteger: true,
            reverseData: true,
            horizontalBars: true,
            height: '500px',
            axisY: {
                offset: 70,
                onlyInteger: true
            },
            axisX: {
                onlyInteger: true
            }
        });

        Session.set('broadcastsuburbs', result);
        Session.set('barchartDataReady', true);

        var broadcasts = [];

    });
  });

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });

  var getBarChartData = Session.get('broadcastsuburbs')


  new Chartist.Bar('.broadcastsuburbs', {getBarChartData}, {
    fullWidth: true,
    showArea: true,
    chartPadding: {
      right: 40
    }
  });


});

Template.broadcastsuburbs.events({
    'change #fromFilter': (event, templateInstance) => {
    event.preventDefault()

    var toDate = new Date($(event.currentTarget).val())

    templateInstance.fromFilter.set(toDate)

    console.log(templateInstance.fromFilter.get())
     
  },
      'change #toFilter': (event, templateInstance) => {
    event.preventDefault()
    var toDate = new Date($(event.currentTarget).val())
    templateInstance.toFilter.set(toDate)
  }
});



Template.broadcastsuburbs.helpers({
    barchartDataReady: function() {

        return Session.get('barchartDataReady')

    }

});
