  import materialize_autocomplete from 'materialize-autocomplete'


  Template.layout.onRendered(function() {




var autocomplete = $('#search').materialize_autocomplete({
    limit: 20,
    multiple: {
        enable: true,
        maxSize: 10,
        onExist: function (item) { /* ... */ },
        onExceed: function (maxSize, item) { /* ... */ }
    },
           dropdown: {
                el: '#singleDropdown',
                itemTemplate: '<li class="ac-item" data-id="<%= item.id %>" data-text=\'<%= item.text %>\'><a href="javascript:void(0)"><%= item.highlight %></a></li>'
            },
    getData: function (value, callback) {
        data = ['data','fake']
        callback(value, data);
    }
});
});