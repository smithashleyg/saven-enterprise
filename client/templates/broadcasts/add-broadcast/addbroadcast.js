Template.addBroadcast.onCreated(function() {
  // We can use the `ready` callback to interact with the map API once the map is ready.
  GoogleMaps.ready('map', function(map) {
    // Add a marker to the map once it's ready

  });


});


Template.adminUserEdit.helpers({
    user: function() {

        var user_id = Router.current().params._id;
        return Meteor.users.findOne({ _id: user_id });

    }
});


Template.addBroadcast.onRendered(function() {





if(GoogleMaps.loaded()){
console.log('true')
 var latLng = getLocation()
GoogleMaps.create({
                    name: 'map',
                    element: document.getElementById('map-container-broadcast'),
                    options: {
                    center: new google.maps.LatLng(latLng.latitude,latLng.longitude),
                        zoom: 15,
                        disableDefaultUI: true,
                        zoomControl: true,
                              styles: [{
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#f5f5f5"
                                }]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [{
                                    "visibility": "off"
                                }]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#F48560"
                                }]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [{
                                    "color": "#f5f5f5"
                                }]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#bdbdbd"
                                }]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#eeeeee"
                                }]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#757575"
                                }]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#e5e5e5"
                                }]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#9e9e9e"
                                }]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#ffffff"
                                }]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#757575"
                                }]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#dadada"
                                }]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#616161"
                                }]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#9e9e9e"
                                }]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#e5e5e5"
                                }]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#eeeeee"
                                }]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [{
                                    "color": "#c9c9c9"
                                }]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [{
                                    "color": "#9e9e9e"
                                }]
                            }
                        ]
                    }
                });

}



       

    

    });


$(document).on({
    'DOMNodeInserted': function() {
        $('.pac-item, .pac-item span', this).addClass('needsclick');
    }
}, '.pac-container');

Template.addBroadcast.events({


    'click #otherSuburbCheckBox': function(e) {
        $("#otherSuburbMap").fadeIn(500);

        // need to stop prop of the touchend event
        $(document).on({
            'DOMNodeInserted': function() {
                $('.pac-item, .pac-item span', this).addClass('needsclick');
            }
        }, '.pac-container');

        // if (!Geolocation.latLng()) {
        //     var latLng = {lat:-31.9505671, lng:115.8580913}
            
        // } else {
        //     var latLng = Geolocation.latLng();
        // }


 GoogleMaps.ready('map', function(map) {

            var input = /** @type {HTMLInputElement} */
                (document.getElementById('broadcastOtherLocation'));

                    var icon = {
            url: '/img/map.icon.orange.png', // url
            scaledSize: new google.maps.Size(30, 30), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

            // map.instance.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.bindTo('bounds', map.instance);
      
            var infowindow = new google.maps.InfoWindow();
             var latLng = getLocation();
             
            var marker = new google.maps.Marker({
                map: map.instance,
                position: new google.maps.LatLng(latLng.latitude,latLng.longitude),
                icon: icon
            });




            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                input.className = '';
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // Inform the user that the place was not found and return.
                    input.className = 'notfound';
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.instance.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setIcon( /** @type {google.maps.Icon} */ ({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                
                var suburbSearch = place.address_components.filter(function(address_component) {
                    return address_component.types.includes("locality");
                });

                //try to find suburb again if it was not found in locality, example try search kwinana wa.
                 if(suburbSearch.length<1){
                    var suburbSearch = place.address_components.filter(function(address_component) {
                    return address_component.types.includes("administrative_area_level_2");
                });
                    }


                var postCodeSearch = place.address_components.filter(function(address_component) {
                    return address_component.types.includes("postal_code");
                });

                var stateSearch = place.address_components.filter(function(address_component) {
                    return address_component.types.includes("administrative_area_level_1");
                });

                var countrySearch = place.address_components.filter(function(address_component) {
                    return address_component.types.includes("country");
                });

                var suburb = suburbSearch.length ? suburbSearch[0].short_name.toLowerCase() : "";
                var postcode = postCodeSearch.length ? postCodeSearch[0].short_name : "";
                var state = stateSearch.length ? stateSearch[0].short_name : "";
                var country = countrySearch.length ? countrySearch[0].short_name : "";
                var lng = place.geometry.location.lng();
                var lat = place.geometry.location.lat();


                Meteor.call('createSuburb', place,lng,lat, function(error, result) {
                    if (error) {
                        console.error('createSuburb(): Unable to create suburb', error)
                    }

                });

                $('#suburb').val(suburb);

                var otherBroadcastLocation = {
                    googleData: place,
                    address: place.formatted_address,
                    suburb: suburb,
                    postcode: postcode,
                    loc: {
                        type: "Point",
                        coordinates: [
                            lng,
                            lat
                        ]

                    }
                }
                Session.set('otherBroadcastLocation', otherBroadcastLocation)

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map.instance, marker);
            });

            setTimeout(function() {
                    google.maps.event.trigger(map.instance, "resize");
                },

                0);

        });


    }
});

Meteor.startup(function() {

    GoogleMaps.load({
        key: 'AIzaSyA3DmTXa_QfEPNRFm8ZaCUsheLbRmkL-68&libraries=places'
    });


});