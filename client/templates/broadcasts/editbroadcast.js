Template.editbroadcast.onCreated( function(){
  var broadcastID = Router.current().params._id;
  this.documents = this.subscribe( 'returnBroadcast' , broadcastID);

    this.subscribe('messagesByBroadcast', broadcastID,{
        onReady: function() {
            if (subscriptionDebug) {
                console.log("SUBSCRIPTION: messagesByBroadcast ready or changed");
            }
        },
        onError: function() {
            if (subscriptionDebug) {
                console.error("SUBSCRIPTION: messagesByBroadcast error", arguments);
            }
        }
    });

});

Template.editbroadcast.onRendered(function() {

                $('.materialboxed').materialbox();


})


Template.editbroadcast.helpers({
    broadcast: function() {

        var broadcastID = Router.current().params._id;

        var returnBroadcast = Broadcasts.findOne({
            _id: broadcastID
        });

        return returnBroadcast

    }
});


Template.editbroadcast.events({
    'click #save': function(event) {
        event.preventDefault();

        let broadcastID = Router.current().params._id;
        let reportType = $('#reportType').val();
        let content = $('#content').val();
        let suburb = $('#suburb').val();
        let createDateTime = $('#createDateTime').val();
        let location = $('#location').val();

        let payload = {
          broadcastID: broadcastID,
          reportType: reportType,
          content: content,
          suburb: suburb,
          createDateTime: createDateTime,
          location: location,
        }
console.log(payload)
       // Meteor.call('editBroadcast', payload);
        //Router.go('/broadcasts');
        Materialize.toast('Broadcast Saved.... don\'t be evil', 4000) // 4000 is the duration of the toast

    },

});
