

Template.broadcasts.onRendered(function() {

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });

    $('#reportType').select2();


});



Template.broadcasts.created = function () {
  this.filter = new ReactiveTable.Filter('greater-than-filter', ['score']);

  this.fromFilter = new ReactiveVar("")
  this.toFilter = new ReactiveVar("")
  this.filterKeywords = new ReactiveVar("")
  this.reportTypeFilter = new ReactiveVar("")

  
};

Template.broadcasts.events({
    'change #fromFilter': (event, templateInstance) => {
    event.preventDefault()
    var fromDate = new Date($(event.currentTarget).val())
    templateInstance.fromFilter.set(fromDate)
  },
      'change #toFilter': (event, templateInstance) => {
    event.preventDefault()
    var toDate = new Date($(event.currentTarget).val())
    templateInstance.toFilter.set(toDate)
  },
    'keyup #filterKeywords': (event, templateInstance) => {
    event.preventDefault()
    var filterKeywords = $(event.currentTarget).val()
    templateInstance.filterKeywords.set(filterKeywords)
  },
    'change #reportType': (event, templateInstance) => {
    event.preventDefault()
    var reportType = $(event.currentTarget).val();
    templateInstance.reportTypeFilter.set(reportType)
  },
});



Template.broadcasts.helpers({
  settings: function () {
    let isAdmin = Meteor.user().profile.isAdmin;
      return {
          showNavigation: 'auto',
          noDataTmpl: 'noDataTmpl',
          rowsPerPage: 10,
          showFilter: false,
          fields: [
                 {
                   key: 'reportType',
                   label: 'Type',
                 },
                 {
                   key: 'content',
                   label: 'Message',
                   fn: function (value, object, key) { 

                    var v = value.replace(/<(?:.|\n)*?>/gm, ' ');

                       if (v.length > 100){
      return v.substring(0,100)+'...';
    }else{return v;}
   
      

                   }


                 },
                 {
                   key: 'geoData.formatted_address',
                   label: 'Location',
                   fn: function (value, object, key) { 

                    var v = value.replace(/<(?:.|\n)*?>/gm, ' ');

                       if (v.length > 100){
      return v.substring(0,100)+'...';
    }else{return v;}
   
      

                   }


                 },
                 {
                   key: 'images',
                   label: 'Images',
                   fn: function (value, object, key) { if(value.image1 || value.image2 || value.image3){return "Yes"}else{return "No"} }

                 },
                 {
                   key: 'fullname',
                   label: 'Broadcaster',
                 },
                 {
                   key: 'suburb',
                   label: 'Suburb',
                    fn: function(value, object, key) { return camelize(value); }

                 },

                 {
                   key: 'createDateTime',
                   label: 'Date / Time', sortOrder: 0, sortDirection: 'descending',
                   fn: function (value, object, key) { return moment(value).format('MM/DD/YYYY, h:mm a');}


                 },
                {
                    key: 'delete',
                    class: 'delete',
                    label: '',
                    fn: function(value) {
                      if(isAdmin){
                        return 'Delete'
                      }
                    }

                },


               ]
                         };
  },

    broadcasts: function () {


 var fromFilter       = Template.instance().fromFilter.get()
 var toFilter         = Template.instance().toFilter.get();
 var filterKeywords   = Template.instance().filterKeywords.get();
 var filterKeywords   = Template.instance().filterKeywords.get();
 var reportTypeFilter = Template.instance().reportTypeFilter.get();


      let query = {
          $or: [{
              suburb: { $regex: new RegExp(filterKeywords, "i") }
          }, {
              content: { $regex: new RegExp(filterKeywords, "i") }
          }, {
              reportType: new RegExp(filterKeywords, 'gi')
          }]
      }

      if (fromFilter != "" && toFilter != "") {
        query["createDateTime"] = { $gte: fromFilter, $lte: toFilter }
      }

      //TO DO
      // if (reportTypeFilter != "") {
      //   query["reportType"] = { $in: reportTypeFilter }
      // }

         
       return Broadcasts.find(query, {sort: {createDateTime: -1}});

}

});


Template.broadcasts.events({
  'click .reactive-table tbody tr': function (event) {
    event.preventDefault();

            if (event.target.className === "delete") {

            Meteor.call('deleteBroadcast', this, function(error, result) {
                if (!error) {
                    swal({
                        title: "Deleted!",
                        text: "Broadcast Deleted",
                        type: "success",
                        timer: 2000
                    });
                } else {
                    console.error(error)
                }
            });

        }else{
         Router.go("/viewbroadcast/"+this._id); 
        }
    

  }
});
