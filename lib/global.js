getGeoCoordinates = function() {
    let organisation = Organisations.findOne({ 'organisation': Meteor.user().profile.organisation }, {
        fields: {
            'polygons': 1
        }
    });

    return organisation.polygons.coordinates;
}