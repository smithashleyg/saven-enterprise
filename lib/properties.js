//When set to true debug messages are written to the local console
SimpleSchema.debug = true

savenEnterpriseVersion = '1.30.0';

debug = true;

//Display collection subscription debug messaging
subscriptionDebug = false;

//When set to true debug messages are written to the server console
debugServer = true;
